class_name Interactable
extends Node2D

const PLAYER_COLLISION_LAYER = 2


signal acknowledged_action(type)


var activation_area


func _ready():
	activation_area = get_node_or_null("ActivationArea")
	if not activation_area:
		push_error(
			"An Interactable needs a child of Area2D named ActivationArea to work! " +
			"Error occured at %s" % self.name
		)
	activation_area.set_collision_mask(PLAYER_COLLISION_LAYER)
	activation_area.connect("area_entered", self, "__on_player_enter")
	activation_area.connect("area_exited", self, "__on_player_leave")

	connect('acknowledged_action', self, '__on_acknowledged_action_wrapper')


func _input(event):	
	if event.is_action_pressed('ui_select'):
		for a in activation_area.get_overlapping_areas():
			if a.get_collision_layer() == PLAYER_COLLISION_LAYER:
				self._on_interact("ui_select")
				return
	elif event.is_action_pressed("ui_put_back"):
		for a in activation_area.get_overlapping_areas():
			if a.get_collision_layer() == PLAYER_COLLISION_LAYER:
				self._on_interact("ui_put_back")
				return


func __on_acknowledged_action_wrapper(type):
	var sound = get_node_or_null('Sound')
	if sound:
		sound.play()
	
	_on_acknowledged_action(type)


func _on_interact(type):
	pass  # to be implemented by the actual scenes


func _on_acknowledged_action(type):
	pass  # to be implemented by the actual scenes


func __on_player_enter(area):
	if area.get_owner().name == "Player":
		$Selection.show()


func __on_player_leave(area):
	if area.get_owner().name == "Player":
		$Selection.hide()
