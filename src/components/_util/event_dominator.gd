extends Node2D


var debugSignals = false


# UI
signal added_nicky_message(text)
signal added_loop_message(text)

# Plate interaction
signal requested_start_plate(sender)
signal requested_pass_plate(target)
signal requested_add_ingredient(ingredient, sender)
signal requested_remove_ingredient(ingredient, sender)
signal requested_swap_plate(plate, sender)
signal reqeusted_pick_sushi(sender)
signal reqeusted_pass_sushi(target)
signal requested_swap_carry(object, sender)
signal requested_clear_belt()


func _ready():
	if debugSignals:
		for s in self.get_signal_list():
			if s.args.empty():
				connect(s.name, self, "__on_signal_no_args", [s.name])
			else:
				connect(s.name, self, "__on_signal_with_args", [s.name])


func __on_signal_no_args(name):
	print('[ED]: got signal %s' % name)


func __on_signal_with_args(args, name):
	print('[ED]: got signal %s with %s' % [name, args])
