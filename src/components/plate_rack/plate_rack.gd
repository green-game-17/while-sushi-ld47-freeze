extends Interactable


func _on_interact(type):
	if type == 'ui_select':
		EventDominator.emit_signal('requested_start_plate', self)
