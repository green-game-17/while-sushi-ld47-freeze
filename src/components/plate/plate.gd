extends Node2D


# pretend these are constants
var INGREDIENT_DATA = {
	'salmon': {
		'texture': load("res://assets/food/raw/salmon.png"),
		'height': 9,
		'offset': 10,
		'multiple_allowed': true
	},
	'shrimp': {
		'texture': load("res://assets/food/raw/shrimp.png"),
		'height': 7,
		'offset': 9,
		'multiple_allowed': true
	},
	'squid': {
		'texture': load("res://assets/food/raw/squid.png"),
		'height': 9,
		'offset': 10,
		'multiple_allowed': true
	},
	'tuna': {
		'texture': load("res://assets/food/raw/tuna.png"),
		'height': 10,
		'offset': 10,
		'multiple_allowed': true
	},
	'carrot': {
		'texture': load("res://assets/food/raw/carrot.png"),
		'height': 4,
		'offset': 8,
		'multiple_allowed': true
	},
	'avacado': {
		'texture': load("res://assets/food/raw/avacado.png"),
		'height': 19,
		'offset': 8,
		'multiple_allowed': true
	},
	'cucumber': {
		'texture': load("res://assets/food/raw/cucumber.png"),
		'height': 5,
		'offset': 12,
		'multiple_allowed': true
	},
	'pepper': {
		'texture': load("res://assets/food/raw/pepper.png"),
		'height': 14,
		'offset': 9,
		'multiple_allowed': true
	},
	'riceball': {
		'texture': load("res://assets/food/raw/riceball.png"),
		'height': 14,
		'offset': 9,
		'multiple_allowed': false
	},
	'nori': {
		'texture': load("res://assets/food/raw/seaweed.png"),
		'height': 6,
		'offset': 12,
		'multiple_allowed': false
	}
}
var RNG = RandomNumberGenerator.new()


signal requested_add_ingredient(ingredient, sender)
signal requested_remove_ingredient(ingredient, sender)


export var rnd_x_range = 5


onready var ingredient_anchor = $IngredientAnchor


var ingredients = []
var nextY = 0


func _ready():
	connect("requested_add_ingredient", self, "__on_add_ingredient")
	connect("requested_remove_ingredient", self, "__on_remove_ingredient")
	RNG.randomize()


func re_set_ingredients(new_list):
	ingredients = []
	nextY = 0

	for c in ingredient_anchor.get_children():
		ingredient_anchor.remove_child(c)

	for ing in new_list:
		__add_ingredient(ing)


func __on_add_ingredient(new_ingredient, sender):
	if ingredients.size() == 5:
		EventDominator.emit_signal("added_nicky_message",
			"Watch out - the tower is getting pretty high.\n" +
			"At most 5 ingredeitns fit on the plate."
		)
		return  # abort

	var did_add_ingredient = __add_ingredient(new_ingredient)
	if did_add_ingredient:
		sender.emit_signal('acknowledged_action', 'add_ingredient')


func __on_remove_ingredient(ingredient, sender):
	if ingredients.size() == 0:
		EventDominator.emit_signal("added_nicky_message",
			"There is nothing on your plate!"
		)
		return
	
	if ingredients[-1] == ingredient:
		var removed_ingredient = ingredients.pop_back()
		ingredient_anchor.get_children()[-1].queue_free()
		var data = INGREDIENT_DATA[removed_ingredient]
		nextY += data.height
		sender.emit_signal('acknowledged_action', 'remove_ingredient')
	else:
		EventDominator.emit_signal("added_nicky_message",
			"Yikes! Only the last ingredient can be removed!"
		)


func __add_ingredient(new_ingredient):
	var data = INGREDIENT_DATA.get(new_ingredient)
	if not data:
		push_error('Ingredient %s not known!' % new_ingredient)

	if not data.get('multiple_allowed') and new_ingredient in ingredients:
		return false  # abort

	ingredients.append(new_ingredient)

	var sprite = Sprite.new()
	sprite.texture = data.get('texture')

	sprite.position.y = nextY - data.get('offset')
	sprite.position.x = RNG.randi_range(1, rnd_x_range)
	if ingredients.size() % 2 == 1:
		sprite.position.x *= -1
	nextY -= data.get('height')

	ingredient_anchor.add_child(sprite)

	return true


func _to_string():
	return '<ingredients: %s>' % str(ingredients)

	
func get_carry_type():
	return 'plate'
