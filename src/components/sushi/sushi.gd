extends Node2D


var type
var main_ingredient
var side_ingredient_1
var side_ingredient_2
var has_nori
var is_super_size


func prepare(_type, _main_ingredient, _side_ingredient_1, _side_ingredient_2, _has_nori):
	type = _type
	main_ingredient = _main_ingredient
	side_ingredient_1 = _side_ingredient_1
	side_ingredient_2 = _side_ingredient_2
	has_nori = _has_nori
	is_super_size = false

	__set_textures()


func prepare_super_size(_type, _main_ingredient, _has_nori):
	type = _type
	main_ingredient = _main_ingredient
	side_ingredient_1 = null
	side_ingredient_2 = null
	has_nori = _has_nori
	is_super_size = true

	__set_textures()


func get_amount_ingredients() -> int:
	if side_ingredient_1:
		if side_ingredient_2:
			return 3
		return 2
	return 1


func __set_textures():
	if type == 'maki' and is_super_size:
		__apply_texture_for_sprite('maki/super/%s' % [main_ingredient], $BaseSprite)
		return
	
	__apply_texture_for_sprite('%s/base/%s' % [type, main_ingredient], $BaseSprite)

	if type == 'maki':
		if side_ingredient_1:
			__apply_texture_for_sprite('maki/side/%s' % side_ingredient_1, $Side1Sprite)
		if side_ingredient_2:
			__apply_texture_for_sprite('maki/side/%s' % side_ingredient_2, $Side2Sprite)
	
	elif type == 'nigiri':
		if has_nori:
			__apply_texture_for_sprite('nigiri/nori', $Side1Sprite)


func __apply_texture_for_sprite(rel_tex_path, sprite):
	var full_tex_path = 'res://assets/food/%s.png' % rel_tex_path
	var tex = load(full_tex_path)
	sprite.texture = tex


func _to_string():
	var type_str = type
	if is_super_size:
		type_str = 'super-' + type_str

	var nori_str = ''
	if has_nori:
		nori_str = 'nori + '

	return '<%s with %s%s + %s + %s>' % [
		type_str, nori_str, main_ingredient, side_ingredient_1, side_ingredient_2
	]


func get_carry_type():
	return 'sushi'
