extends Area2D


var customers = {
	'top-right': null,
	'middle-right': null,
	'bottom-right': null,
	'top-left': null,
	'middle-left': null,
	'bottom-left': null
}


func _ready():
	randomize()


func _on_new_customer(customer):
	var available_seats = []
	for seat in customers:
		if not customers[seat]:
			available_seats.append(seat)
	if available_seats.size() > 0:
		var seat = available_seats[randi() % available_seats.size()]
		match seat:
			'top-right':
				customer.position.x = $TopRightSeat.position.x + (randi() % 20) - 10
				customer.position.y = $TopRightSeat.position.y + (randi() % 10) - 5
			'middle-right':
				customer.position.x = $MiddleRightSeat.position.x + (randi() % 20) - 10
				customer.position.y = $MiddleRightSeat.position.y + (randi() % 10) - 5
			'bottom-right':
				customer.position.x = $BottomRightSeat.position.x + (randi() % 20) - 10
				customer.position.y = $BottomRightSeat.position.y + (randi() % 10) - 5
			'top-left':
				customer.position.x = $TopLeftSeat.position.x + (randi() % 20) - 10
				customer.position.y = $TopLeftSeat.position.y + (randi() % 10) - 5
				customer.flip_h = true
			'middle-left':
				customer.position.x = $MiddleLeftSeat.position.x + (randi() % 20) - 10
				customer.position.y = $MiddleLeftSeat.position.y + (randi() % 10) - 5
				customer.flip_h = true
			'bottom-left':
				customer.position.x = $BottomLeftSeat.position.x + (randi() % 20) - 10
				customer.position.y = $BottomLeftSeat.position.y + (randi() % 10) - 5
				customer.flip_h = true
		customers[seat] = customer
		$CustomerSort.add_child(customer)
		customer.sitting_down(self, seat)


func _has_favorite_ingredient(sushi) -> String:
	for seat in customers:
		if customers[seat]:
			if customers[seat].has_ingredient(sushi):
				return seat
	return ''


func _on_sushi_entered(beltObjectArea):
	var sushi = beltObjectArea.get_owner().sushi
	var foundSeat = self._has_favorite_ingredient(sushi)
	if foundSeat != '':
		beltObjectArea.get_owner().remove_sushi()
		customers[foundSeat].set_sushi(sushi)
		beltObjectArea.get_owner().delete_this()


func has_free_seats() -> bool:
	for seat in customers:
		if not customers[seat]:
			return true
	return false


func get_sushi_position(seat) -> Vector2:
	match seat:
		'top-right':
			return get_node("TopRightSeatFoodPosition").position
		'middle-right':
			return get_node("MiddleRightSeatFoodPosition").position
		'bottom-right':
			return get_node("BottomRightSeatFoodPosition").position
		'top-left':
			return get_node("TopLeftSeatFoodPosition").position
		'middle-left':
			return get_node("MiddleLeftSeatFoodPosition").position
		'bottom-left':
			return get_node("BottomLeftSeatFoodPosition").position
	return Vector2(0, 0)


func get_satisfaction_level() -> float:
	var satisfaction_level = 0
	var customer_count = 0
	for seat in customers:
		if customers[seat]:
			satisfaction_level += customers[seat].satisfaction
			customer_count += 1
	if customer_count > 0:
		return satisfaction_level / customer_count
	return -1.0
