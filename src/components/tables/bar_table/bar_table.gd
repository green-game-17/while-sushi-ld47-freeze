extends Node2D


var customers = {
	'1to4': {
		'1': null,
		'2': null,
		'3': null,
		'4': null
	},
	'5to8': {
		'5': null,
		'6': null,
		'7': null,
		'8': null
	},
	'9to11': {
		'9': null,
		'10': null,
		'11': null
	},
	'12to15': {
		'12': null,
		'13': null,
		'14': null,
		'15': null
	}
}


# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()


func _on_new_customer(customer):
	var availableSeats = []
	for group in customers:
		for seat in customers[group]:
			if not customers[group][seat]:
				availableSeats.append(seat)
	if availableSeats.size() > 0:
		var seat_no = availableSeats[randi() % availableSeats.size()]
		customer.position.x = get_node('Seat' + seat_no).position.x + (randi() % 20) - 10
		customer.position.y = get_node('Seat' + seat_no).position.y + (randi() % 10) - 5
		
		match seat_no as int:
			1, 2, 3, 4:
				customers['1to4'][seat_no] = customer
			5, 6, 7, 8:
				customers['5to8'][seat_no] = customer
				customer.flip_h = true
			9, 10, 11:
				customers['9to11'][seat_no] = customer
				customer.flip_h = true
			12, 13, 14, 15:
				customers['12to15'][seat_no] = customer
				customer.flip_h = true
		$CustomerSort.add_child(customer)
		customer.sitting_down(self, seat_no)


func _has_favorite_ingredient(group, sushi) -> String:
	for seat in customers[group]:
		if customers[group][seat]:
			if customers[group][seat].has_ingredient(sushi):
				return seat
	return ''


func _on_Seats1To4Area_entered(beltObjectArea):
	var sushi = beltObjectArea.get_owner().sushi
	var foundSeat = self._has_favorite_ingredient('1to4', sushi)
	if foundSeat != '':
		beltObjectArea.get_owner().remove_sushi()
		customers['1to4'][foundSeat].set_sushi(sushi)
		beltObjectArea.get_owner().delete_this()


func _on_Seats5To8Area_entered(beltObjectArea):
	var sushi = beltObjectArea.get_owner().sushi
	var foundSeat = self._has_favorite_ingredient('5to8', sushi)
	if foundSeat != '':
		beltObjectArea.get_owner().remove_sushi()
		customers['5to8'][foundSeat].set_sushi(sushi)
		beltObjectArea.get_owner().delete_this()


func _on_Seats9To11Area_entered(beltObjectArea):
	var sushi = beltObjectArea.get_owner().sushi
	var foundSeat = self._has_favorite_ingredient('9to11', sushi)
	if foundSeat != '':
		beltObjectArea.get_owner().remove_sushi()
		customers['9to11'][foundSeat].set_sushi(sushi)
		beltObjectArea.get_owner().delete_this()


func _on_Seats12To15Area_entered(beltObjectArea):
	var sushi = beltObjectArea.get_owner().sushi
	var foundSeat = self._has_favorite_ingredient('12to15', sushi)
	if foundSeat != '':
		beltObjectArea.get_owner().remove_sushi()
		customers['12to15'][foundSeat].set_sushi(sushi)
		beltObjectArea.get_owner().delete_this()


func has_free_seats() -> bool:
	for group in customers:
		for seat in customers[group]:
			if not customers[group][seat]:
				return true
	return false


func get_sushi_position(seat_no) -> Vector2:
	return get_node('Seat' + seat_no + 'FoodPosition').position


func get_satisfaction_level() -> float:
	var satisfaction_level = 0
	var customer_count = 0
	for group in customers:
		for seat in customers[group]:
			if customers[group][seat]:
				satisfaction_level += customers[group][seat].satisfaction
				customer_count += 1
	if customer_count > 0:
		return satisfaction_level / customer_count
	return -1.0
