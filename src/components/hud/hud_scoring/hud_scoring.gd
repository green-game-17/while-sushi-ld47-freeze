extends Control


onready var happiness_sprite = get_node('Scalar/HappinessSprite')
onready var rating_sprites = [
	get_node('Scalar/RatingSprite/RatingSprite1'),
	get_node('Scalar/RatingSprite/RatingSprite2'),
	get_node('Scalar/RatingSprite/RatingSprite3'),
	get_node('Scalar/RatingSprite/RatingSprite4'),
	get_node('Scalar/RatingSprite/RatingSprite5')
]
onready var money_label = get_node('MoneyLabel')

onready var happiness_texs = [
	load("res://assets/ui/hud/satisfaction1.png"),
	load("res://assets/ui/hud/satisfaction2.png"),
	load("res://assets/ui/hud/satisfaction3.png"),
	load("res://assets/ui/hud/satisfaction4.png"),
	load("res://assets/ui/hud/satisfaction5.png")
]
onready var star_empty_tex = load("res://assets/ui/hud/starempty.png")
onready var star_half_tex = load("res://assets/ui/hud/starhalf.png")
onready var star_full_tex = load("res://assets/ui/hud/starfull.png")


func _process(_delta):
	var happiness = GameManager.get_current_satisfaction()  # naming, am i right?
	var happines_int = min(5, max(1, round(happiness))) # integer in (1..5)
	happiness_sprite.texture = happiness_texs[happines_int - 1]

	var rating = GameManager.get_current_rating()
	for i in range(5):
		var j = 5 - i
		var sprite = rating_sprites[j - 1]
		var rating_fraction = rating - j + 1

		if rating_fraction >= 1:
			sprite.texture = star_full_tex
		elif rating_fraction >= 0.5:
			sprite.texture = star_half_tex
		else:
			sprite.texture = star_empty_tex
	
	var money = GameManager.get_current_money()
	var money_str = str(money)
	if money >= 1_000_000_000:
		money_str = 'a lot'
	elif money >= 1_000_000:
		money_str = '%s %03d %03d' % [
			floor(money / 1_000_000), floor((money % 1_000_000) / 1_000), money % 1_000
		]
	elif money >= 1_000:
		money_str = '%s %03d' % [floor(money / 1_000), money % 1_000]
	money_label.text = money_str
