extends Control

onready var parent = get_parent()
onready var timer = $Timer
onready var label = get_node('ColorRect/Label')


func _ready():
	EventDominator.connect("added_loop_message", self, "__on_add_message")
	timer.connect("timeout", self, "__on_timeout")
	__hide()


func __hide():
	self.visible = false


func __show():
	self.visible = true


func __on_add_message(text):
	label.text = text
	__show()
	timer.start()


func __on_timeout():
	__hide()
