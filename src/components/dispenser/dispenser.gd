extends Interactable


export var item_name = ''
export var costs = 0


func _on_interact(type):
	match type:
		"ui_select":
			EventDominator.emit_signal('requested_add_ingredient', item_name, self)
		"ui_put_back":
			EventDominator.emit_signal('requested_remove_ingredient', item_name, self)


func _on_acknowledged_action(type):
	match type:
		'add_ingredient':
			print(costs)
			GameManager.reduce_money(costs)
		'remove_ingredient':
			GameManager.add_money(round(costs / 2))
