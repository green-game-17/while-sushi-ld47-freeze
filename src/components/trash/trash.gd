extends Interactable


signal passed_carry(obj)


func _ready():
	connect('passed_carry', self, '__on_pass_carry')


func _on_interact(type):
	if type == 'ui_select':
		EventDominator.emit_signal('requested_swap_carry', null, self)


func __on_pass_carry(trashed_obj):
	if trashed_obj:
		emit_signal('acknowledged_action', 'add_ingredient')
