extends "./player_movement.gd"


signal passed_sushi(sushi)


onready var anchor_up = $BackAnchor
onready var anchor_down = $FrontAnchor
onready var anchor_left = $LeftAnchor
onready var anchor_right = $RightAnchor
onready var current_anchor = $FrontAnchor


var plate
var sushi


func _ready():
	EventDominator.connect('requested_start_plate', self, '__on_new_plate')
	EventDominator.connect('requested_add_ingredient', self, '__on_add_ingredient')
	EventDominator.connect('requested_remove_ingredient', self, '__on_remove_ingredient')
	EventDominator.connect('requested_pass_plate', self, '__on_pass_plate')
	EventDominator.connect('requested_swap_plate', self, '__on_swap_plate')
	EventDominator.connect('reqeusted_pick_sushi', self, '__on_pick_sushi')
	connect('passed_sushi', self, '__on_passed_sushi')
	EventDominator.connect('reqeusted_pass_sushi', self, '__on_pass_sushi')
	EventDominator.connect('requested_swap_carry', self, '__on_swap_carry')


func _input(event):
	if Input.is_action_pressed("zoom_in"):
		$Camera2D.zoom.x = max($Camera2D.zoom.x - 0.1, 0.5)
		$Camera2D.zoom.y = max($Camera2D.zoom.y - 0.1, 0.5)
	elif Input.is_action_pressed('zoom_out'):
		$Camera2D.zoom.x = min($Camera2D.zoom.x + 0.1, 2)
		$Camera2D.zoom.y = min($Camera2D.zoom.y + 0.1, 2)


func __update_anchor_for_velocity(velocity):
	var old_anchor = current_anchor
	
	if velocity.x < 0:
		current_anchor = anchor_left
	elif velocity.x > 0:
		current_anchor = anchor_right
	elif velocity.y < 0:
		current_anchor = anchor_up
	elif velocity.y > 0:
		current_anchor = anchor_down

	var carry = null
	if plate:
		carry = plate
	elif sushi:
		carry = sushi

	if carry:
		old_anchor.remove_child(carry)
		current_anchor.add_child(carry)


func __on_add_ingredient(ingredient, sender):
	if not plate:
		EventDominator.emit_signal("added_nicky_message",
			"Hmh, you need a plate to carry ingredients.\nPlease grab one first."
		)
		return

	if GameManager.has_enough_money(sender.costs):
		plate.emit_signal("requested_add_ingredient", ingredient, sender)
	else:
		EventDominator.emit_signal("added_nicky_message",
			"You need more money to buy this."
		)


func __on_remove_ingredient(ingredient, sender):
	if plate:
		plate.emit_signal("requested_remove_ingredient", ingredient, sender)


func __on_new_plate(sender):
	if plate:
		return
	
	var plateScene = load("res://components/plate/plate.tscn")
	plate = plateScene.instance()
	current_anchor.add_child(plate)

	sender.emit_signal('acknowledged_action', 'add_ingredient')


func __on_swap_plate(new_plate, sender):
	if plate:
		current_anchor.remove_child(plate)

	if sender and sender.has_signal('passed_plate'):
		sender.emit_signal('passed_plate', plate)

	current_anchor.add_child(new_plate)
	plate = new_plate


func __on_pass_plate(target):
	if not plate:
		return

	var current_plate = plate
	plate = null

	current_anchor.remove_child(current_plate)
	if target.has_signal('passed_plate'):
		target.emit_signal('passed_plate', current_plate)


func __on_pick_sushi(target):
	if plate:
		EventDominator.emit_signal("added_nicky_message",
			"Oh, the counter looks pretty full. Serve the sushi on it first."
		)
		return
	
	if sushi:
		EventDominator.emit_signal("added_nicky_message",
			"Oh, your hands look pretty full already. Place the sushi you are carrying first."
		)
		return

	if target.has_signal('reqeusted_pass_sushi'):
		target.emit_signal('reqeusted_pass_sushi', self)


func __on_passed_sushi(new_sushi):
	current_anchor.add_child(new_sushi)
	sushi = new_sushi


func __on_pass_sushi(target):
	if not sushi:
		return
	
	var leaving_sushi = sushi
	sushi = null

	current_anchor.remove_child(leaving_sushi)

	target.emit_signal('passed_sushi', leaving_sushi)


func __on_swap_carry(obj, sender):
	var leaving_obj	= null
	if plate:
		leaving_obj = plate
		plate = null
	elif sushi:
		leaving_obj = sushi
		sushi = null

	if leaving_obj:
		current_anchor.remove_child(leaving_obj)

	if sender and sender.has_signal('passed_carry'):
		sender.emit_signal('passed_carry', leaving_obj)
	
	if obj:
		if obj.get_carry_type() == 'plate':
			plate = obj
		elif obj.get_carry_type() == 'sushi':
			sushi = obj
		current_anchor.add_child(obj)
