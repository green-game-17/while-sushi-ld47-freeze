extends Interactable


export var rotate_anchor = false


signal passed_carry(obj)


var anchor
var carry = null


func _ready():
	connect('passed_carry', self, '__on_pass_carry')

	var sprite = get_node('Working Area')
	anchor = Node2D.new()
	anchor.scale.x = sprite.scale.x
	anchor.scale.y = sprite.scale.y
	sprite.add_child(anchor)


func _on_interact(type):
	if type == 'ui_select':
		var leaving_carry = null
		if carry:
			leaving_carry = carry
			anchor.remove_child(leaving_carry)
			carry = null
			
		EventDominator.emit_signal('requested_swap_carry', leaving_carry, self)


func __on_pass_carry(new_carry):
	if new_carry:
		carry = new_carry
		anchor.add_child(new_carry)
