extends '../work_counter.gd'


func _ready():
	anchors = [
		$Anchor1, $Anchor2
	]


func _on_passed_plate(plate):
	if not plate:
		return

	if not 'riceball' in plate.ingredients:
		__deny_plate("Hmh, you can't prepare sushi without rice! Go grab some.", plate)
		return

	var has_nori = 'nori' in plate.ingredients

	var dish_ingredients = []
	for ingredient in plate.ingredients:
		if not ingredient in ['riceball', 'nori']:
			dish_ingredients.append(ingredient)
	
	if dish_ingredients.empty():
		__deny_plate("Hmh, you can't prepare sushi without topping! Go grab some.", plate)
		return

	emit_signal('acknowledged_action', 'add_ingredient')

	for a in anchors:
		var new_sushi = sushi_scene.instance()
		new_sushi.prepare('nigiri',
			dish_ingredients[0], null, null, has_nori
		)
		a.add_child(new_sushi)
		sushis.append(new_sushi)

	var return_ingredients = dish_ingredients.slice(1, dish_ingredients.size() - 1)

	if not return_ingredients.empty():
		plate.re_set_ingredients(return_ingredients)
		EventDominator.emit_signal('requested_swap_plate', plate, null)
