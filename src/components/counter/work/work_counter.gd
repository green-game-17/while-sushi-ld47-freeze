extends Interactable


signal passed_plate(plate)
signal reqeusted_pass_sushi(target)


onready var sushi_scene = load("res://components/sushi/sushi.tscn")


var anchors = []
var sushis = []


func _ready():
	connect('passed_plate', self, '_on_passed_plate')
	connect('reqeusted_pass_sushi', self, '__on_pass_sushi')


func _on_interact(type):
	if type == 'ui_select':
		if sushis.empty():
			EventDominator.emit_signal('requested_pass_plate', self)
		else:
			EventDominator.emit_signal('reqeusted_pick_sushi', self)
	

func _on_passed_plate(_plate):
	pass


func __deny_plate(msg, plate):
	EventDominator.emit_signal('added_nicky_message', msg)
	EventDominator.emit_signal('requested_swap_plate', plate, null)


func __on_pass_sushi(target):
	if sushis.empty():
		return

	var leaving_sushi = sushis.pop_back()
	anchors[sushis.size()].remove_child(leaving_sushi)
	target.emit_signal('passed_sushi', leaving_sushi)
