extends PathFollow2D


signal is_deleted(belt_object)

var sushi


func _ready():
	add_child(sushi)


func set_connection(sushiBelt):
	self.connect('is_deleted', sushiBelt, '_on_belt_object_is_deleted')


func remove_sushi():
	remove_child(sushi)

func delete_this():
	emit_signal('is_deleted', self)
